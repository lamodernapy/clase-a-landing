<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="gfx/favicon.png" /> 

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/clasea.css?v=2.5">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />


    <title>Clase A</title>



    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KPPPVKC');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KPPPVKC"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->



    <?php 
        if (isset($_GET['seccion'])){
            $seccion_actual=$_GET['seccion'];

            switch($seccion_actual){
              case "summer": 
                $img="model_plus_summer.png";
                $galeria = array("summer4_chico.jpg", "summer5_chico.jpg", "summer6_chico.jpg", "summer1_chico.jpg", "summer2_chico.jpg", "summer3_chico.jpg", "summer8_chico.jpg", "summer9_chico.jpg", "summer10_chico.jpg");
                $mensaje = array(
                  "el%20traje%20de%20baño%20MADG55220",
                  "el%20traje%20de%20baño%20MADG55135",
                  "la%20bermuda%20BEGHG51783%20ESTAMPADO",
                  "camisas%20CAHG53677%20MARRON,%20CAHG53669%20AZUL,%20CAHG51781%20AMARILLO%20y%20la%20REHG52312%20REMERA,%20SHHG51770%20SHORT",
                  "el%20traje%20de%20baño%20rojo%20MADG54267%20y%20el%20traje%20de%20baño%20azul%20MADG54679",
                  "la%20camisa%20CADG51811%20RAYAS,%20CADG54218%20ESTRELLAS%20y%20el%20short%20SHDG51766",
                  "la%20BEGHG51783%20ESTAMPADO,%20BEHG53727%20AZUL",
                  "el%20%20short%20SHDG53761%20NEGRO,%20SHDG51815%20AMARILLO",
                  "el%20SHHG51784%20azul,%20SHHG54076%20coral"
                );
                $galeria_grande = array("summer4_grande.jpg", "summer5_grande.jpg", "summer6_grande.jpg", "summer1_grande.jpg", "summer2_grande.jpg", "summer3_grande.jpg", "summer8_grande.jpg", "summer9_grande.jpg", "summer10_grande.jpg");
                $mainclass = "summer";
                break;

              case "hombres": 
                $img="model_plus_hombres.png";
                $galeria = array("hombres_1.jpg", "hombres_2.jpg", "hombres_3.jpg", "hombres_4.jpg", "hombres_5.jpg", "hombres_6.jpg");
                $mensaje = array(
                  "la%20camisa%20CAHG53994,%20bermuda%20BEHG51891",
                  "la%20camisa%20CAHG51773,%20bermuda%20BEGD51769",
                  "la%20camisa%20CAHG51773",
                  "la%20remera%20Polo%20POHG53834",
                  "la%20remera%20Polo%20POHG53834",
                  "la%20camisa%20CAHG51781,%20bermuda%20BEHG53365"
                );
                $galeria_grande = array("hombres_1_grande.jpg", "hombres_2_grande.jpg", "hombres_3_grande.jpg", "hombres_4_grande.jpg", "hombres_5_grande.jpg", "hombres_6_grande.jpg");
                $mainclass = "hombres";
                break;

              default: 
                //default es mujeres
                $img ="model_plus.png";
                $galeria = array("mujeres_1.jpg", "mujeres_2.jpg", "mujeres_3.jpg" , "mujeres_4.jpg", "mujeres_5.jpg", "mujeres_6.jpg", "mujeres_7.jpg", "mujeres_8.jpg", "mujeres_9.jpg");
                $mensaje = array(
                  "la%20camisa%20CADG54218%20con%20jeans%20VADG51681",
                  "el%20vestido%20VEDG54396",
                  "la%20blusa%20BLDG54401%20short%20SHDG51766",
                  "la%20blusa%20BLDG54420%20short%20SHDG51764",
                  "la%20remera%20REDG54411%20y%20VADG51681%20jeans",
                  "la%20camisa%20CADG51811%20y%20el%20jeans%20VADG51683",
                  "la%20blusa%20BLDG53563",
                  "la%20blusa%20BLDG53563,%20bermuda%20BEDG51805",
                  "el%20short%20SHDG53761",);
                $galeria_grande = array("mujeres_1_grande.jpg", "mujeres_2_grande.jpg", "mujeres_3_grande.jpg", "mujeres_4_grande.jpg", "mujeres_5_grande.jpg", "mujeres_6_grande.jpg", "mujeres_7_grande.jpg", "mujeres_8_grande.jpg", "mujeres_9_grande.jpg");
                $mainclass = "mujeres";
            }
          }else{
            $img ="model_plus.png";
                $galeria = array("mujeres_1.jpg", "mujeres_2.jpg", "mujeres_3.jpg" , "mujeres_4.jpg", "mujeres_5.jpg", "mujeres_6.jpg", "mujeres_7.jpg", "mujeres_8.jpg", "mujeres_9.jpg");
                $mensaje = array(
                  "la%20camisa%20CADG54218%20con%20jeans%20VADG51681",
                  "el%20vestido%20VEDG54396",
                  "la%20blusa%20BLDG54401%20short%20SHDG51766",
                  "la%20blusa%20BLDG54420%20short%20SHDG51764",
                  "la%20remera%20REDG54411%20y%20VADG51681%20jeans",
                  "la%20camisa%20CADG51811%20y%20el%20jeans%20VADG51683",
                  "la%20blusa%20BLDG53563",
                  "la%20blusa%20BLDG53563,%20bermuda%20BEDG51805",
                  "el%20short%20SHDG53761",);
                $galeria_grande = array("mujeres_1_grande.jpg", "mujeres_2_grande.jpg", "mujeres_3_grande.jpg", "mujeres_4_grande.jpg", "mujeres_5_grande.jpg", "mujeres_6_grande.jpg", "mujeres_7_grande.jpg", "mujeres_8_grande.jpg", "mujeres_9_grande.jpg");
                $mainclass = "mujeres";
          }
         ?>

    <section class="jumbo">
      <div class="plus_model" style="background-image: url('gfx/<?php echo ($img); ?>');">
        <div class="container">
          <div class="row">
            
            <div class="col-md-8 col-lg-7 col-xl-6 col_promo justify-content-center align-items-center">
              <img src="gfx/logo_clase_a.svg" class="img-fluid logo" />
              <ul class="navsecciones <?php echo $mainclass; ?>">
                <li class="mujeres">
                  <a href="?seccion=mujeres">Mujeres &rarr; </a>
                </li>
                <li class="hombres">
                  <a href="?seccion=hombres">Hombres &rarr; </a>
                </li>
                
                <li class="summer">
                  <a href="?seccion=summer">Summer &rarr; </a>
                </li>
              </ul>

              <img src="gfx/nuevacoleccion.png" class="img-fluid promo" />



              <div class="row w-100 cont_botones">
                <div class="col-12">
                  <a href="javascript:void(0)" id="verlocales_link" class="btn btn-secondary conoce">
                    <img src="gfx/pin_naranja.svg" class="img-fluid" />
                    <span>Conocé<br> los locales</span>
                  </a>
                  <a href="https://wa.me/595986209921" class="btn btn-primary whatsapp" target="_blank">
                    <img src="gfx/whatsapp.svg" class="img-fluid" />
                    <span>Contactanos<br> vía whatsapp</span>
                  </a>
                  
                </div><!-- col -->
              </div><!-- row -->
            </div><!-- col -->
          </div><!-- row -->
        </div><!-- container -->
      </div><!-- plus_model -->
    </section><!-- section-->

    <section class="galeria">
      <div class="container cont_galeria">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6">
              <h2>¡No te pierdas las prendas de la nueva colección!</h2>
            </div><!-- col -->
        </div><!-- row --> 
        <div class="row">
          <div class="col-12 col-md-4 text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[0]; ?>">
              <img src="gfx/<?php echo $galeria[0]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[0] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4 text-center ">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[1]; ?>">
              <img src="gfx/<?php echo $galeria[1]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[1] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4  text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[2]; ?>">
              <img src="gfx/<?php echo $galeria[2]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[2] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
        </div><!-- row -->

        <?php //if($seccion_actual== "summer" || $seccion_actual == "mujeres" || $seccion_actual == "hombres" || $seccion_actual == ""){?>

          <div class="row">
          <div class="col-12 col-md-4 text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[3]; ?>">
              <img src="gfx/<?php echo $galeria[3]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[3] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4  text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[4]; ?>">
              <img src="gfx/<?php echo $galeria[4]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[4] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4  text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[5]; ?>">
              <img src="gfx/<?php echo $galeria[5]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[5] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
        </div><!-- row -->


       <?php //  } ?> 



       <?php if( $seccion_actual == "summer" || $seccion_actual == "mujeres" || $seccion_actual == "" ){?>

          <div class="row">
          <div class="col-12 col-md-4 text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[6]; ?>">
              <img src="gfx/<?php echo $galeria[6]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[6] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4  text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[7]; ?>">
              <img src="gfx/<?php echo $galeria[7]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[7] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
          <div class="col-12 col-md-4  text-center">
            <a class="unafoto" data-fancybox="gallery" href="gfx/<?php echo $galeria_grande[8]; ?>">
              <img src="gfx/<?php echo $galeria[8]; ?>" class="img-fluid" />
            </a>
            <a href="https://wa.me/595986209921?text=Quiero%20saber%20sobre%20<?php echo $mensaje[8] ?>%20de%20la%20landing%20Clase%20A" class="btn btn-primary whatsapp mr-auto ml-auto" target="_blank">
              <img src="gfx/whatsapp.svg" class="img-fluid" />
              <span>¡Quiero saber más!</span>
            </a>
          </div><!-- col -->
        </div><!-- row -->


       <?php  } ?> 

      </div><!-- container -->
    </section><!-- galeria -->


    <section class="locales">
      <div class="container">
        <div class="row align-items-center mb-5">
            <div class="col-12 col-md-6">
              <h6>Conocé nuestros locales</h6>
            </div><!-- col -->
            <div class="col-12 col-md-6">

              <select class="selectorciudad custom-select custom-select-lg" name="selectorciudad">
                <option value="all" data-filter="all" selected>Todas las ciudades</option>
               
                <option value="asuncion" data-filter="asuncion">Asunción</option>
                
                <option value="cde" data-filter="cde">Ciudad del Este</option>
                <option value="encarnacion" data-filter="encarnacion">Encarnacion</option>
                
                <option value="lomapyta" data-filter="lomapyta">Loma Pytá</option>
                <option value="limpio" data-filter="limpio">Limpio</option>
                <option value="luque" data-filter="luque">Luque</option>
                <option value="sanlorenzo" data-filter="sanlorenzo">San Lorenzo</option>
                
              </select>


            </div><!-- col -->
        </div><!-- row --> 

        <div class="row listadorow achicado">
         
          <div class="col-12 col-sm-6 filter asuncion">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Asunción:</b><br>Avda. Fernando de la Mora c/ Médicos Del Chaco</li>
                <li class="telefono">0981506532</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter asuncion">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Asunción:</b><br>Avda. Eusebio Ayala c/ Solar Guaraní</li>
                <li class="telefono">0981506465</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter asuncion">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Asunción:</b><br>Avda. Sacramento 261 c/ Mcal. Lopez</li>       
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter asuncion">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Asunción:</b><br>Charles de Gaulle c/ Hassler</li>       
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6  filter cde">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Ciudad del Este:</b><br>Avda. Bernardino Caballero 892 e/ 29 de Setiembre</li>
                <li class="telefono">0981858277</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter encarnacion">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Encarnación:</b><br>Juan L. Mallorquin c/ Villarrica 1520</li>
                <li class="telefono">0981506077</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6  filter limpio">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Limpio:</b><br>Fulgencio Yegros y Mcal. Estigarribia 153</li>
                <li class="telefono">0981505763</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter lomapyta">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Loma Pytá:</b><br>Ruta Transchaco km 12 casi Tomás Ruffinelli</li>
                <li class="telefono">0981506806</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6  filter luque">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Luque:</b><br>Valeriano Zevallos c/ Cerro Corá</li>
                <li class="telefono">0982132429</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->
          <div class="col-12 col-sm-6 filter sanlorenzo">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>San Lorenzo:</b><br>J.M. Cueto c/ Hernandarias</li>
                <li class="telefono">0981506511</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col -->

          
          
          <?php /*
          <div class="col-12 col-sm-6 filter fernando">
            <div class="cadainfo d-flex align-items-center">
              <ul>
                <li class="direccion"><b>Fernando OUTLET (Tienda de Saldos):</b><br>Avda. Mcal.Estigarribia 868 e/Saturio Ríos</li>
                <li class="telefono">0982132487</li>
              </ul>
            </div><!-- cadainfo -->
          </div><!-- col --> */ ?>
        </div><!-- row -->
        <div class="row" >
          <div class="col-12 text-center">
            <a href="javascript:void(0)" id="botonlocales" class="btn btn-primary">
              Ver más
            </a>
            <a href="javascript:void(0)" class="btn btn-primary" id="botonlocalescerrar" style="display: none">
              Ver menos
            </a>
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container -->
    </section>

    <footer>
      <div class="container cont_footer">
        <div class="row">
            <div class="col-12 text-center">
              <img src="gfx/logo_clase_a_f_negro.svg" class="img-fluid" />
            </div><!-- col -->
        </div><!-- row --> 
      </div><!-- container -->
    </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="" crossorigin="anonymous"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

        <script>
      $(document).ready(function(){

        $('#verlocales_link').click(function(){
              $('html, body').animate({
                  scrollTop: $( ".locales" ).offset().top-0
              }, 1000);
            });


        $(".selectorciudad").change(function(){

            $("#botonlocalescerrar, #botonlocales").hide();
            $(".listadorow").removeClass("achicado");
            $(".listadorow").addClass("agrandado");

            var value = $("select option:selected").attr('data-filter');
            
            if(value == "all")
            {
                //$('.filter').removeClass('hidden');
                $('.filter').show('1000');
            }
            else
            {
    //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
    //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');
                
            }
        });
        
            if ($(".filter-button").removeClass("active")) {
            $(this).removeClass("active");
            }
            $(this).addClass("active");

            });

            $("#botonlocales").click(function() {
              $(".listadorow").removeClass("achicado");
              $(".listadorow").addClass("agrandado");
              $(this).hide();
              $("#botonlocalescerrar").show();
            });
            $("#botonlocalescerrar").click(function() {
              $(".listadorow").removeClass("agrandado");
              $(".listadorow").addClass("achicado");
              $(this).hide();
              $("#botonlocales").show();
            });
            
            

      
        </script>



  </body>
</html>